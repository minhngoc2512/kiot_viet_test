## I.Trong Laravel, khi 1 request được thực hiện sẽ bắt đầu từ đâu, các luồng chạy như thế nào?

Khi có một request đến, file ``public/index.php`` sẽ được chạy đầu tiên.

- Bước 1: Kiểm tra xem ứng dụng có được bảo trì hay không bằng cách kiểm tra file ``storage/framework/maintenance.php``
  có tồn
  tại hay không. Nếu có thì laravel sẽ trả ra thông báo lỗi hoặc chuyển hướng tới một url đã được chỉ định.
   ```php 
   if (file_exists($maintenance = __DIR__.'/../storage/framework/maintenance.php')) {
       require $maintenance;
   } 
   ```
- Bước 2: Gọi đến file ``vendor/autoload.php`` để tải các thư viện được đăng ký trong file ``composer.json``.
- Bước 3: Gọi đến file ``bootstrap/app.php``, trong đó:
    - Tạo một instance từ Laravel Application. Laravel sẽ tạo ra các ``global path``
      như ``base_path``,``storage_path``,``config_path``,... cho các thư mục tương ứng và đăng kí
      các ``Service Provider``, class ``Alias``
      như ``Route`` ``Log`` ``Event``
      ```php 
       $app = new Illuminate\Foundation\Application(
           $_ENV['APP_BASE_PATH'] ?? dirname(__DIR__)
       );
      ```
    - Đăng ký binding cho các class ``Http\Kernel`` ``Console\Kernel`` ``Exception`` để phục vụ cho việc xử lý request,
      command hay mỗi khi có lỗi xảy ra
      ```php 
        $app->singleton(
          Illuminate\Contracts\Http\Kernel::class,
          App\Http\Kernel::class
        );

        $app->singleton(
          Illuminate\Contracts\Console\Kernel::class,
          App\Console\Kernel::class
        );

        $app->singleton(
          Illuminate\Contracts\Debug\ExceptionHandler::class,
          App\Exceptions\Handler::class
        );
      ```
- Bước 3: Lúc này Laravel sẽ chạy qua các middleware(middleware, groupMiddleware, routeMiddleware) để lọc request. Sau
  đó đi qua ``Router`` ``Controller, Blade Template`` (nếu được khai báo ở router) và trả về kết quả.

  ```php 
    $response = $kernel->handle(
        $request = Request::capture()
    )->send();
  ```
- Bước 4: Trả kết quả cho người dùng, đóng phiên và kết thúc:

  ```php
  $response->send();
  $kernel->terminate($request, $response);

  ```

### II. Trong dự án A có dùng Respository, vậy repository là gì? tại sao phải dùng Repository? Repository khi đươc viết thì có đang áp dụng OOP không? Vì sao?

- ``Repository`` là phần trung gian, ở giữa phần dữ liệu và phần xử lý logic. Nơi lưu trữ logic truy vấn dữ
  liệu, ``Controller`` sẽ gọi vào ``Repository`` để lấy dữ liệu.
- Khi dùng ``Repository`` code dễ đọc hơn, dễ phát triển sản phẩm trong làm việc nhóm, giảm thay đổi code khi phần mềm
  có thay đổi cấu trúc dữ liệu, tránh việc lặp code, hạn chế lỗi truy vấn và dễ dàng kiểm tra code.
- ``Repository`` khi đươc viết thì có đang áp dụng ``OOP`` vì ``OOP`` cũng là phương pháp lập trình tránh việc lặp code,
  kế thừa các hàm chức năng, tập trung vào một đối tượng, vấn đề cần xử lý trong class đúng với tiêu chí ``Respository``
  đề ra.

### III. Trong Laravel, hãy cho biết những hiểu biết của bạn về queue

- Queue là các công việc được thực hiện tách biệt khỏi các luồng chính có thể chạy ngầm. Trì hoãn các công việc tốn thời
  hay tài nguyên như gửi mail, xử lý file lớn, gửi thông báo tới bên khác ngoài hệ thống,... Từ đó giúp tăng trải nghiệm
  người dùng, tránh ảnh hưởng đến SEO của trang web.
- Queue trong laravel bạn có thể chỉ định độ ưu tiên cho job nào được thực hiện trước hay trì hoãn nó.
- Có thể dùng nhiều loại driver để lưu queue như: database, redis,...

### IV. Trong database, có 1 bảng "Products" gồm 2 field là tên, giá, cụ thể như sau:

sp 1: 10,
sp 2: 9,
sp 3: 8,
sp 4: 9,
sp 5: 8
sp 6: 7,
sp 7: 10
sp 8: 1,
sp 9: 2
sp 10: 5

- Câu hỏi 1: Lấy ra tất cả các sản phẩm có giá cao thứ 3

  Để lấy ra các sản phẩm, ta viết câu sql theo tiêu chí:
    - Tìm khoảng giá cao thứ 3
    - Lấy sản phẩm theo khoảng giá đó

  ```sql
    SELECT name , price
    FROM products
    WHERE price = (SELECT DISTINCT price
                  FROM products
                  ORDER BY price DESC
                  LIMIT 2, 1)
    ORDER BY price DESC;
  ```
- Câu hỏi 2: Xóa các sản phẩm trùng nhau về giá, mà chỉ giữ lại 1 sản phẩm, tức là ví dụ sp 1 và sp 7 thì xóa sp 7, giữ
  sp 1

  Để xóa các sản phẩm trùng nhau về giá, ta viết câu sql theo tiêu chí sau:
    - Lấy ra id một sản phẩn duy nhất với mỗi khoảng giá.
    - Đưa danh sách id đó vào bảng tạm vì MySQL không cho phép cập nhật bảng mà bạn đang sử dụng trong phần chọn bên
      trong làm tiêu chí xóa.
    - Xóa các sản phẩm có trong bảng ngoại trừ các id sản phẩm đã lấy ra ở bước trên, Ví dụ câu query bên dưới:
  ```sql 
  DELET FROM san_pham
   WHERE id NOT IN (
     select id from ( 
        SELECT MIN(id) as id FROM san_phan GROUP BY gia
       ) as bang_tam
      );

  ```

- Câu hỏi 3: Nếu bảng này có 500k bản ghi và bạn phải cập nhật dữ liệu cho 500k với mỗi record 1 giá tri khác nhau, thì
  bạn sẽ xử lý thế nào?

  Trong trường hợp này sễ chia nhỏ dữ liệu ra để cập nhật, mỗi lần như thế cập nhật từ 500 đến 1000 bản ghi một lúc
  trong một câu query có định dạng như bên dưới:

Ví dụ giá trị cập nhật lưu theo dạng:

| id      | value_update   |
|---------|----------------|
| id_sp_1 | value_update_1 |
| id_sp_2 | value_update_2 |
| ...     | ...            |

Ví dụ: Cập nhật 500 bản ghi trong một lần

  ```sql
    UPDATE san_pham
    SET col_update = case
                         when id = id_sp_1 then value_update_1
                         when id = id_sp_2 then value_update_2
                             ...
                             when id = id_sp_500 then value_update_500 end
    where id in (id_sp_1, ..., id_sp_500);
  ```

Tùy cấu hình máy chủ đang chạy database mà lựa chọn số lượng bản ghi cập nhật một lúc cho phù hợp, tránh hiện tượng
mysql gây chậm máy chủ.

### V. (Câu này dành cho các bạn đã từng làm VueJS) Các bạn đã làm vue nhiều, vậy hãy cho biết vòng đời của vue?

Các vòng đời của vuejs là:

- Giai đoạn khởi tạo:
    - ``beforeCreate``: Bước này thư viện vuejs được khởi tạo
    - ``created``: Data và event được thiết lập
      ```javascript
      new Vue({
          el: "#app",
          data: {
              content: "Lifecycle Hooks",
          },
          beforeCreate () {
              console.log('before create')
              console.log(this.content) // khi này this.content trả về undefined vì data chưa được nhận (observe).
          },
          created () {
              console.log('created')
              console.log(this.content) // khi này this.content trả về  "Lifecycle Hooks" vì quá trình observe data, thiết lập event đã hoàn thành.
          }
      }) 
      ```
- Giai đoạn gắn kết DOM
    - ``beforeMount``: Data, event được thiết lập và trước khi gắn kết vào DOM nên chưa truy cập được các phần tử trong
      DOM.
    - ``mounted``: Ở giao đoạn này DOM đã được thiết lập và có thể truy cập:
      ```javascript
      new Vue({
          el: "#app",
          data: {
              content: "Lifecycle Hooks",
          },
          beforeMount () {
              console.log('before mount')
              console.log(this.$el.textContent) // lỗi vì chưa gắn kết DOM vì vậy chưa thể truy cập đến các thành phần trong DOM ở console.log sẽ hiển thị ra 
          },
          mounted () {
              console.log('mounted')
              console.log(this.$el.textContent) // khi này this.$el đã gắn kết với DOM, lúc này có thể truy cập được tới các thành phần trong DOM 
          }
      })
  ```
- Giai đoạn cập nhật DOM khi dữ liệu thay đổi:
    - ``beforeUpdate``: Khi dữ liệu thay đổi và trước khi được render hiển thị lại cho người dùng
    - ``updated``: Lúc này có thể truy cập DOM với giá trị mới được cập nhật
- Giai đoạn hủy:

    - ``beforeDestroy``: Giai đoạn trước khi instance bị hủy. Đây là nơi để quản lý tài nguyên xóa tài nguyên, dọn dẹp
      các component.
  ```javascript
  var vm = new Vue({
    el: "#app",
    data: {
        content: "Lifecycle Hooks"
    },
    beforeDestroy () {
         this.content = null
         delete this.content
    },
  })

  ```
    - ``destroyed``: Giai đoạn này mọi thành phần đã hủy bỏ hết

  ```javascript
  var vm = new Vue({
    el: "#app",
    data: {
        content: "Lifecycle Hooks"
    },
    destroyed () {
        console.log(this) //thời điểm này sẽ không nhận được gì ở console.
    },
  })

  ```

### VI . Cho 2 mảng có lần lượt m và n phần tử

- Nêu giải thuật tìm các phần tử chung của 2 mảng đó;
    - Giả sử mảng có m phần tử là ``A`` và n phần tử là ``B``. Để tìm các phần tử chung của 2 mảng ``A`` và ``B`` có thể
      thực hiện theo các bước sau:
        - Khởi tạo một mảng trống ``C`` để chứa các phần tử chung.
        - Kiểm tra số phần tử trong mảng để ưu tiên duyệt mảng nào có ít phần tử nhất.
        - Ví dụ trong trường này mảng ``A`` có ít phần tử hơn mảng ``B``.Khi đó, duyệt từng phần tử của mảng ``A``, nếu
          phần tử đó là phần tử của mảng ``B`` và chưa có trong mảng ``C``, ta thêm phần tử đó vào mảng ``C``.
        - Trả về mảng ``C`` chứa các phần tử chung của hai mảng.
    
- Tính xem giải thuật bạn sử dụng có độ phức tạp bằng bao nhiêu.``( Chưa có đáp án )``